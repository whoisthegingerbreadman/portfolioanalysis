import os
import json
from datetime import datetime

date_str_format = "%m/%d/%Y"


class Config(object):
    def __init__(self):
        # The algorithm to be used to make trading decisions.
        self.algorithm = None

        # The assets that can be traded in the simulation.
        self.available_assets = []

        # The initial state of the portfolio when trading begins.
        self.initial_state = {}

        # The desired source for financial data.
        self.source = None

        # The amount of capital that can be used for trading.
        self.capital = 0

        # The time at which the trading simulation begins.
        self.start_time = None

        # The time at which the trading simulation ends.
        self.end_time = None

    def read_from_file(self, filename):
        config_dir = os.path.join(os.path.dirname(__file__), 'portfolios')
        config_path = os.path.join(config_dir, filename + '.json')
        with open(config_path, 'r') as f:
            config = json.loads(f.read())

        self.start_time = datetime.strptime(
            config['env']['start_time'], date_str_format)
        if config['env']['end_time'] == 'now':
            self.end_time = datetime.now()
        else:
            self.end_time = datetime.strptime(
                config['env']['end_time'], date_str_format)

        # todo: add thing for time delta parameter

        self.source = config['source']
        self.capital = int(config['capital'])
        self.algorithm = config['algorithm']

        self.available_assets = config['env']['available_assets']
        self.initial_state = config['initial_state']

        self.analyzers = config['analyzers']
        self.hash = json.dumps(str(config['env']), sort_keys=True).encode()

    def to_dict(self):
        start_time = self.start_time.strftime(date_str_format)
        end_time = self.end_time.strftime(date_str_format)
        return {
            'start_time': start_time,
            'end_time': end_time,
            'algorithm': self.algorithm,
            'source': self.source,
            'capital': self.capital,
            'initial_state': self.initial_state,
            'available_assets': self.available_assets
        }
