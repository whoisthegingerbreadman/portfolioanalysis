""" Factory class that returns instantiated analyzers given analyzer names. """

import os
from datetime import datetime

from algorithms.static_algorithm.StaticAlgorithm import StaticAlgorithm
from algorithms.random_algorithm.RandomAlgorithm import RandomAlgorithm


class AlgorithmFactory(object):
    algorithms = {StaticAlgorithm.name: StaticAlgorithm,
                  RandomAlgorithm.name: RandomAlgorithm}

    def get_algorithm(self, name):
        return self.algorithms[name]()
