""" Holds the initial state of the portfolio over the entire backtrace."""

class StaticAlgorithm(object):
    name = "static"

    def __init__(self, context = None):
        pass
    
    def step(self, timestamp, previous_portfolio, price_data):
        # Just hold the current assets.
        return previous_portfolio