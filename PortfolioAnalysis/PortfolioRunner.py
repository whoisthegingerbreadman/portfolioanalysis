from datetime import datetime, timedelta
from collections import defaultdict
from DataReader import DataReaderFactory
from PortfolioState import PortfolioState
from algorithms.AlgorithmFactory import AlgorithmFactory
from copy import deepcopy
import os


class PortfolioRunner(object):
    """ Runs a backtrace over some number of days in the past. """

    def __init__(self, config, credential_path='credentials/td_credentials.json'):
        # Configuration holds several values relevant to the run.
        self.config = config

        # Get all relevant price data.
        if config.hash in os.listdir('data'):
            print('Data already exists')
        data_reader = DataReaderFactory().create_reader(config.source)
        self.price_data = data_reader.get_daily_prices(
            config.available_assets, config.start_time, config.end_time)

        # Derive timestamps from price data.
        self.times = self.price_data.get_times()

        # Holds a set of portfolio states for each timestep.
        self.portfolio_states = defaultdict(lambda: PortfolioState())

        # Load the initial state from the config file.
        self.load_initial_state(config)

        # Keeps track of the current step of the runner.
        self.step_index = 1

        # Algorithm that performs the trade decision making.
        self.algorithm = AlgorithmFactory().get_algorithm(config.algorithm)

    def load_initial_state(self, config):
        start_time = self.times[0]
        portfolio_state = PortfolioState(
            self.times[0], self.price_data, config.capital)

        for asset, percent in self.config.initial_state.items():
            price = self.price_data.get_asset_price_at_time(
                asset, 'open', start_time)
            capital = (percent / 100.0) * self.config.capital
            shares = int(capital / price)
            portfolio_state.place_order(asset, shares)
        self.portfolio_states[start_time] = portfolio_state

    def run(self):
        while self.step_index < len(self.times):
            self.step()

    def step(self):
        previous_timestamp = self.times[self.step_index - 1]
        current_timestamp = self.times[self.step_index]

        previous_portfolio = self.portfolio_states[previous_timestamp]
        historic_prices = self.price_data.get_prices_until_time(
            current_timestamp)
        portfolio = self.algorithm.step(
            current_timestamp, previous_portfolio, historic_prices)

        self.portfolio_states[current_timestamp] = deepcopy(portfolio)

        self.step_index += 1
