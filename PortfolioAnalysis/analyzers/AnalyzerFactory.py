""" Factory class that returns instantiated analyzers given analyzer names. """

import os
from datetime import datetime

from analyzers.basic_view.basic_view import BasicView
from analyzers.config_analyzer.config_analyzer import ConfigAnalyzer


class AnalyzerFactory(object):
    analyzers = {BasicView.name: BasicView,
                 ConfigAnalyzer.name: ConfigAnalyzer}

    def get_analyzers(self, names, runner):
        archive_folder = datetime.now().strftime("%m_%d_%Y_%H_%M_%S")
        os.mkdir(os.path.join('archive', archive_folder))
        analyzers = [ConfigAnalyzer(runner, archive_folder=archive_folder)]
        for name in names:
            analyzer = self.analyzers[name](runner, archive_folder)
            analyzers.append(analyzer)
        return analyzers
