import json
import os

class ConfigAnalyzer(object):
    name = 'config_analyzer'

    def __init__(self, runner, archive_folder=''):
        self.runner = runner
        self.archive_folder = archive_folder
        self.config_file_name = "config.json"

    def start(self):
        pass

    def step(self):
        pass

    def done(self):
        config = self.runner.config
        with open(os.path.join('archive', self.archive_folder, self.config_file_name), 'w') as f:
            json.dump(config.to_dict(), f)
