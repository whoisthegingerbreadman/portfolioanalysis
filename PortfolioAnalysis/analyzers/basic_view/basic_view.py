import matplotlib.pyplot as plt
import numpy as np
import os

class BasicView(object):
    name = 'basic_view'

    def __init__(self, runner, archive_folder=''):
        self.runner = runner
        self.archive_folder = archive_folder

    # not sure if analyzers need a start and step, but will keep for now.
    def start(self):
        pass

    def step(self):
        pass

    def done(self):
        times = self.runner.times
        values = []
        for time in times:
            portfolio = self.runner.portfolio_states[time]
            value = 0
            for asset, shares in portfolio.shares.items():
                price = self.runner.price_data.get_asset_price_at_time(asset, 'open', time)
                value += price * shares
            values.append(value)

        plt.plot_date(times, values, linestyle='solid')

        timestamps = [time.timestamp() for time in times]
        trendline = np.poly1d(np.polyfit(timestamps, values, 1))(timestamps)
        plt.plot(times, trendline, "r--")

        plt.xlabel('Date')
        plt.ylabel('Portfolio Value ($)')
        plt.savefig(os.path.join(
            'archive', self.archive_folder, 'basic_view.jpg'))