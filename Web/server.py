from flask import Flask, render_template
import os
import json

app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')


@app.route('/index.js')
def main_page_javascript():
    with open('index.js', 'r') as f:
        return f.read()


@app.route('/archive.json')
def archive_data():
    all_out = get_archive_structure('archive', '')
    return all_out


def get_archive_structure(loc, dir_name=''):
    all_out = {}
    if dir_name:
        dir_loc = os.path.join(loc, dir_name)
    else:
        dir_loc = loc
    for a, b, c in os.walk(dir_loc):
        all_out[a] = []
        for f in c:
            all_out[a].append(f)
        for d in b:
            all_out[a].append(get_archive_structure(a, d))
    return all_out


if __name__ == '__main__':
    app.run()
