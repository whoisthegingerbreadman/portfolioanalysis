#!/usr/bin/env python3

from datetime import datetime
from datetime import timedelta

from DataReader import TDAmeritradeReader
from PortfolioRunner import PortfolioRunner
from Config import Config
#from visualizer import visualize_runner
import argparse
import os
import json
from analyzers.AnalyzerFactory import AnalyzerFactory

CREDENTIALS_PATH = "/Users/galois/Documents/git/trading/PortfolioAnalysis/credentials/td_credentials.json"


def make_argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--Portfolio",
                        help="portfolio config", default="test_portfolio")
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    args = make_argument_parser()

    config = Config()
    config.read_from_file(args.Portfolio)

    runner = PortfolioRunner(config)
    runner.run()

    analyzers = AnalyzerFactory().get_analyzers(config.analyzers, runner)

    for analyzer in analyzers:
        analyzer.done()
