""" Holds the initial state of the portfolio over the entire backtrace."""
import numpy as np


class RandomAlgorithm(object):
    name = "random"

    def __init__(self, context=None):
        pass

    def step(self, timestamp, previous_portfolio, price_data):
        # Just hold the current assets.
        random_ticker = np.random.choice(list(price_data.keys()))
        total_liquidity = previous_portfolio.liquidity
        max_movement = total_liquidity//price_data[random_ticker].iloc[-1]['open']
        num_held_stock = previous_portfolio.get_held_stock(random_ticker)
        print(num_held_stock, max_movement, random_ticker)
        num_stocks = np.random.randint(-1*num_held_stock, max_movement+1)
        previous_portfolio.place_order(random_ticker, num_stocks)
        return previous_portfolio
