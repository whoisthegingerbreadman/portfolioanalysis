import os
import json
from collections import defaultdict


class PortfolioState(object):
    """ Container class for the current state of a portfolio at a given timestep. 
        Also enforces trading rules for the algorithm, and provides trading helpers.
    """

    def __init__(self, timestamp, price_data, liquidity):
        # Number of shares held in each asset.
        self.shares = defaultdict(lambda: 0)

        # Local undetstanding of the prices of each asset.
        self.price_data = price_data.get_prices_at_time(timestamp)

        # Amount of free capital to buy assets.
        self.liquidity = liquidity

    def read_from_account(self):
        # todo: Add td ameritrade support to get current portfolio.
        pass

    def get_asset_names(self):
        return self.keys()

    def place_order(self, asset, shares):
        """ Places a buy/sell order for an asset. """
        if shares > 0:
            price = float(self.price_data[asset]['open'])
            if shares * price > self.liquidity:
                shares = self.liquidity//price
            self.shares[asset] += shares
            self.liquidity -= price * shares
            return True
        if shares < 0:
            price = float(self.price_data[asset]['open'])
            if shares > self.shares[asset]:
                shares = self.shares[asset]//1
            self.shares[asset] += shares
            self.liquidity -= price * shares
            return True

    def remove_asset_percent(self, asset, percent):
        pass

    def get_held_stock(self, ticker):
        if ticker in self.shares:
            return self.shares[ticker]
        else:
            return 0

    def add_asset_percent(self, asset, percent):
        percent = max(percent, 100)
        pass

    # @property
    # def portfolio_value(self):
    #     total =
    #     for asset in self.shares:

    #         self.price_data[asset]['open']
