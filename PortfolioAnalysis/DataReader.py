import json
import pandas as pd
from datetime import datetime
import numpy as np
from collections import defaultdict
import yfinance as yf
import tqdm
from datetime import datetime, timedelta


from td.client import TDClient
import yfinance as yf


def generate_data(assets, start, end):
    reader = TDAmeritradeReader()
    return reader.get_daily_prices(assets, start, end)


class DataReaderFactory(object):
    data_readers = {}

    def register_data_reader(reader):
        DataReaderFactory.data_readers[reader.name] = reader

    def __init__(self):
        pass

    def create_reader(self, source, credential_path=None):
        if credential_path:
            return self.data_readers[source](credential_path)
        else:
            return self.data_readers[source]()


class PriceContainer(defaultdict):
    def __init__(self):
        defaultdict.__init__(self, lambda: pd.DataFrame(
            columns=['time', 'open', 'close', 'low', 'high', 'volume']))

    def __len__(self):
        for k in self:
            return len(self[k])
        return 0

    def to_numpy(self):
        return np.array([prices.to_numpy() for _, prices in self.items()])

    def get_times(self, index=None):
        if index is None:
            for _, prices in self.items():
                return prices['time']
        elif index >= len(self):
            return -1
        else:
            for _, prices in self.items():
                return prices['time'][index]
        return []
    
    # This should die.
    def get_prices(self, price_type):
        return np.array([np.array(prices[price_type]) for _, prices in self.items()])
    
    #todo: have this return a new PriceContainer
    def get_prices_at_time(self, time):
        """ Returns a mapping of all prices for all available assets at a given point in time. """
        ret = {}
        for asset, _ in self.items():
            ret[asset] = self.get_asset_prices_at_time(asset, time) 
        return ret
    
    #todo: have this return a new PriceContainer
    def get_prices_until_time(self, time):
        ret = {}
        for asset, _ in self.items():
            ret[asset] = self[asset].loc[self[asset].time <= time]
        return ret
    
    def get_asset_price_at_time(self, asset, price_type, time):
        """ Returns a specific price type for a given asset at a point in time. """
        return float(self[asset].loc[self[asset].time == time][price_type])

    def get_asset_prices_at_time(self, asset, time):
        """ Returns the prices for a given asset at a point in time. """
        return self[asset].loc[self[asset].time == time]
    


class DataReader(object):
    def __init__(self):
        pass

    def get_daily_prices(self, assets, start, end):
        pass

    def get_minute_prices(self, assets, start, end):
        pass


@DataReaderFactory.register_data_reader
class TDAmeritradeReader(DataReader):
    name = 'TD'

    def __init__(self, credential_path='credentials/td_credentials.json'):
        with open(credential_path, 'r') as f:
            credentials = json.loads(f.read())

        self.session = TDClient(client_id=credentials["client_id"],
                                redirect_uri=credentials["redirect_uri"],
                                credentials_path=credentials["credentials_path"])
        self.session.login()

    def get_daily_prices(self, assets, start, end):
        price_data = PriceContainer()
        for asset in assets:
            prices_raw = self.session.get_price_history(
                symbol=asset,
                start_date=self.encode_datetime(start),
                end_date=self.encode_datetime(end),
                period_type='month',
                frequency_type='daily',
                frequency=1
            )
            self.append_to_container(price_data, asset, prices_raw)
        return price_data

    def get_minute_prices(self, assets, start, end):
        price_data = PriceContainer()
        for asset in assets:
            prices_raw = self.session.get_price_history(
                symbol=asset,
                start_date=self.encode_datetime(start),
                end_date=self.encode_datetime(end),
                period_type='daily',
                frequency_type='minute',
                frequency=1
            )
            self.append_to_container(price_data, asset, prices_raw)
        return price_data

    def encode_datetime(self, time):
        return str(int(round(time.timestamp() * 1000)))

    def append_to_container(self, container, asset, prices_raw):
        for candle in prices_raw['candles']:
            candle['time'] = datetime.fromtimestamp(
                int(candle['datetime'] / 1000))
            del candle['datetime']
            container[asset] = container[asset].append(
                candle, ignore_index=True)


@DataReaderFactory.register_data_reader
class YahooFinanceReader(DataReader):
    name = 'YF'

    def __init__(self):
        pass

    def get_daily_prices(self, assets, start, end):
        price_data = PriceContainer()
        for asset in tqdm.tqdm(assets):
            data = yf.download(asset, start, end, interval='1d')
            data = data.reset_index().set_index(np.arange(0, len(data), 1),).rename(columns={
                'Open': 'open', 'Close': 'close', 'High': 'high', 'Low': 'low', 'Volume': 'volume', 'Date': 'time'})
            del data['Adj Close']
            price_data[asset] = data
        return price_data

    def get_minute_prices(self, assets, start, end):
        pass


def read_data(asset_names, start, end, interval='1d', source='TD', credential_path=""):
    if source == "TD":
        reader = TDAmeritradeReader(credential_path=credential_path)
    elif source == "YF":
        reader = YahooFinanceReader()
    if interval == '1d':
        price_data = reader.get_daily_prices(asset_names, start, end)
    # put more stuff here
    return price_data
